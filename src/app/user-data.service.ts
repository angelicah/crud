
import {User} from './user-data';
//import { Details } from './ProductDetails';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { UserFetch } from './user-fetch';


export class UserData implements InMemoryDbService {
  createDb(){
    const users: User[] = [
      { id: 1, name: 'Pen'},
      { id: 2, name: 'Pencil'},
      { id: 3, name: 'Table'},
      { id: 4, name: 'Chair'},
      { id: 5, name: 'Bed'}

    ];
    const user: UserFetch[]=[
      {
        id:1,
        name: 'Pen',
      },
      {
       id:2,
       name: 'Pencil',
     }    ,
     {
       id:3,
       name: 'Table',
     }    ,
     {
       id:4,
       name: 'Chair',
     } ,
     {
       id:5,
       name: 'Bed',
     }
     ];

    return {users, user};
  }
}
